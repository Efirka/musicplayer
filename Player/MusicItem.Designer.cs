﻿namespace Player
{
    partial class MusicItem
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicItem));
            this.musicArea = new System.Windows.Forms.TableLayoutPanel();
            this.musicItemImage = new System.Windows.Forms.PictureBox();
            this.title = new System.Windows.Forms.Label();
            this.description = new System.Windows.Forms.Label();
            this.musicArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.musicItemImage)).BeginInit();
            this.SuspendLayout();
            // 
            // musicArea
            // 
            this.musicArea.BackColor = System.Drawing.Color.Transparent;
            this.musicArea.ColumnCount = 1;
            this.musicArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.musicArea.Controls.Add(this.musicItemImage, 0, 0);
            this.musicArea.Controls.Add(this.title, 0, 1);
            this.musicArea.Controls.Add(this.description, 0, 2);
            this.musicArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicArea.Location = new System.Drawing.Point(0, 0);
            this.musicArea.Margin = new System.Windows.Forms.Padding(10);
            this.musicArea.Name = "musicArea";
            this.musicArea.Padding = new System.Windows.Forms.Padding(20);
            this.musicArea.RowCount = 3;
            this.musicArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.musicArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.musicArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.musicArea.Size = new System.Drawing.Size(230, 310);
            this.musicArea.TabIndex = 0;
            // 
            // musicItemImage
            // 
            this.musicItemImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicItemImage.Image = ((System.Drawing.Image)(resources.GetObject("musicItemImage.Image")));
            this.musicItemImage.Location = new System.Drawing.Point(23, 23);
            this.musicItemImage.Name = "musicItemImage";
            this.musicItemImage.Size = new System.Drawing.Size(184, 184);
            this.musicItemImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.musicItemImage.TabIndex = 0;
            this.musicItemImage.TabStop = false;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.title.Font = new System.Drawing.Font("Candara", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.title.ForeColor = System.Drawing.Color.White;
            this.title.Location = new System.Drawing.Point(23, 210);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(184, 40);
            this.title.TabIndex = 1;
            this.title.Text = "label1";
            // 
            // description
            // 
            this.description.AutoSize = true;
            this.description.BackColor = System.Drawing.Color.Transparent;
            this.description.Dock = System.Windows.Forms.DockStyle.Fill;
            this.description.Font = new System.Drawing.Font("Candara Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.description.ForeColor = System.Drawing.Color.DarkGray;
            this.description.Location = new System.Drawing.Point(23, 250);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(184, 40);
            this.description.TabIndex = 2;
            this.description.Text = "label2";
            // 
            // MusicItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.Controls.Add(this.musicArea);
            this.Name = "MusicItem";
            this.Size = new System.Drawing.Size(230, 310);
            this.musicArea.ResumeLayout(false);
            this.musicArea.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.musicItemImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel musicArea;
        private PictureBox musicItemImage;
        private Label title;
        private Label description;
    }
}
