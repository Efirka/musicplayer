﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Player
{
    public partial class MusicVolume : UserControl
    {
        float volume = 50f;
        EventHandler? valueChange;


        public event EventHandler ValueChanged
        {
            add { valueChange += value; }
            remove { valueChange -= value; }
        }
        public float Value { get; private set; }
        
        public MusicVolume()
        {
            InitializeComponent();
            trackBarItem.ValueChanged += TrackBarItem_ValueChanged;
            trackBarItem.SetDuration(1);
            trackBarItem.Value = volume;
            WheelRecursive(this);
            HoverRecursive(this);
            Value = 0.5f;

        }

        private void TrackBarItem_ValueChanged(object? sender, EventArgs e)
        {
            double volumeDouble = (double)sender!;
            volume = (float)volumeDouble! * 100;
            if (valueChange != null)
            {
                Value = volume / 100;
                valueChange.Invoke(volume/100, e);
            }
            trackBarItem.Value = volume;
        }

        #region Wheel
        void WheelRecursive(Control element)
        {
            element.MouseWheel += Element_MouseWheel; 
            
            foreach (Control child in element.Controls) 
            { 
                WheelRecursive(child); 
            }
                       
        }

        private void Element_MouseWheel(object? sender, MouseEventArgs e)
        {
            if(e.Delta >0)
            {
                volume += 1;
            }
            else
            {
                volume -= 1;
            }

            if (volume < 0)
            {
                volume = 0;
                volumePicture.Image = Properties.Resources.mute;
            }
            else
            {
                volumePicture.Image = Properties.Resources.unmute;
            }

            if (volume > 100)  volume = 100;

            trackBarItem.Value = volume;

            if (valueChange != null)
            {
                Value = volume / 100;
                valueChange.Invoke(volume / 100, e);
            }
        }

        #endregion

        #region Hover
        void HoverRecursive(Control element)
        {
            element.MouseEnter += Element_MouseEnter;
            element.MouseLeave += Element_MouseLeave;

            foreach (Control child in element.Controls)
            {
                HoverRecursive(child);
            }
        }

        private void Element_MouseLeave(object? sender, EventArgs e)
        {
            trackBarItem.ProgressLineColor = Color.Silver;
        }

        private void Element_MouseEnter(object? sender, EventArgs e)
        {
            trackBarItem.ProgressLineColor = Color.White;
        }
        #endregion
    }
}
