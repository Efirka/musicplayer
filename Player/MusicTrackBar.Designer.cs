﻿namespace Player
{
    partial class MusicTrackBar
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MusicTrackBar));
            this.musicTrackBarArea = new System.Windows.Forms.TableLayoutPanel();
            this.playlistArea = new System.Windows.Forms.TableLayoutPanel();
            this.currentTrackImage = new System.Windows.Forms.PictureBox();
            this.textArea = new System.Windows.Forms.TableLayoutPanel();
            this.title = new System.Windows.Forms.Label();
            this.musicControlsArea = new System.Windows.Forms.TableLayoutPanel();
            this.musicButtons = new System.Windows.Forms.TableLayoutPanel();
            this.play = new Player.CustomButton();
            this.previousTrack = new Player.CustomButton();
            this.nextTrack = new Player.CustomButton();
            this.trackBarArea = new System.Windows.Forms.TableLayoutPanel();
            this.endTime = new System.Windows.Forms.Label();
            this.currentTime = new System.Windows.Forms.Label();
            this.trackBarItem = new Player.TrackBarItem();
            this.volumeArea = new System.Windows.Forms.TableLayoutPanel();
            this.musicVolume = new Player.MusicVolume();
            this.musicPlayTimer = new System.Windows.Forms.Timer(this.components);
            this.musicTrackBarArea.SuspendLayout();
            this.playlistArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.currentTrackImage)).BeginInit();
            this.textArea.SuspendLayout();
            this.musicControlsArea.SuspendLayout();
            this.musicButtons.SuspendLayout();
            this.trackBarArea.SuspendLayout();
            this.volumeArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // musicTrackBarArea
            // 
            this.musicTrackBarArea.BackColor = System.Drawing.Color.Transparent;
            this.musicTrackBarArea.ColumnCount = 3;
            this.musicTrackBarArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.musicTrackBarArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.musicTrackBarArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.musicTrackBarArea.Controls.Add(this.playlistArea, 0, 0);
            this.musicTrackBarArea.Controls.Add(this.musicControlsArea, 1, 0);
            this.musicTrackBarArea.Controls.Add(this.volumeArea, 2, 0);
            this.musicTrackBarArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicTrackBarArea.Location = new System.Drawing.Point(0, 0);
            this.musicTrackBarArea.Name = "musicTrackBarArea";
            this.musicTrackBarArea.RowCount = 1;
            this.musicTrackBarArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.musicTrackBarArea.Size = new System.Drawing.Size(996, 85);
            this.musicTrackBarArea.TabIndex = 0;
            // 
            // playlistArea
            // 
            this.playlistArea.ColumnCount = 2;
            this.playlistArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.playlistArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playlistArea.Controls.Add(this.currentTrackImage, 0, 0);
            this.playlistArea.Controls.Add(this.textArea, 1, 0);
            this.playlistArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playlistArea.Location = new System.Drawing.Point(3, 3);
            this.playlistArea.Name = "playlistArea";
            this.playlistArea.Padding = new System.Windows.Forms.Padding(10);
            this.playlistArea.RowCount = 1;
            this.playlistArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playlistArea.Size = new System.Drawing.Size(194, 79);
            this.playlistArea.TabIndex = 2;
            // 
            // currentTrackImage
            // 
            this.currentTrackImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.currentTrackImage.Image = global::Player.Properties.Resources.images;
            this.currentTrackImage.Location = new System.Drawing.Point(10, 10);
            this.currentTrackImage.Margin = new System.Windows.Forms.Padding(0);
            this.currentTrackImage.Name = "currentTrackImage";
            this.currentTrackImage.Size = new System.Drawing.Size(60, 59);
            this.currentTrackImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.currentTrackImage.TabIndex = 0;
            this.currentTrackImage.TabStop = false;
            // 
            // textArea
            // 
            this.textArea.ColumnCount = 1;
            this.textArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.textArea.Controls.Add(this.title, 0, 0);
            this.textArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textArea.Location = new System.Drawing.Point(70, 10);
            this.textArea.Margin = new System.Windows.Forms.Padding(0);
            this.textArea.Name = "textArea";
            this.textArea.RowCount = 1;
            this.textArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.textArea.Size = new System.Drawing.Size(114, 59);
            this.textArea.TabIndex = 1;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.title.Font = new System.Drawing.Font("Candara", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.title.ForeColor = System.Drawing.SystemColors.Control;
            this.title.Location = new System.Drawing.Point(3, 0);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(108, 59);
            this.title.TabIndex = 0;
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // musicControlsArea
            // 
            this.musicControlsArea.BackColor = System.Drawing.Color.Transparent;
            this.musicControlsArea.ColumnCount = 1;
            this.musicControlsArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.musicControlsArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.musicControlsArea.Controls.Add(this.musicButtons, 0, 0);
            this.musicControlsArea.Controls.Add(this.trackBarArea, 0, 1);
            this.musicControlsArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicControlsArea.Location = new System.Drawing.Point(203, 3);
            this.musicControlsArea.Name = "musicControlsArea";
            this.musicControlsArea.RowCount = 2;
            this.musicControlsArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.musicControlsArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.musicControlsArea.Size = new System.Drawing.Size(590, 79);
            this.musicControlsArea.TabIndex = 0;
            // 
            // musicButtons
            // 
            this.musicButtons.BackColor = System.Drawing.Color.Transparent;
            this.musicButtons.ColumnCount = 5;
            this.musicButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.musicButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.musicButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.musicButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.musicButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.musicButtons.Controls.Add(this.play, 2, 0);
            this.musicButtons.Controls.Add(this.previousTrack, 1, 0);
            this.musicButtons.Controls.Add(this.nextTrack, 3, 0);
            this.musicButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicButtons.Location = new System.Drawing.Point(3, 3);
            this.musicButtons.Name = "musicButtons";
            this.musicButtons.RowCount = 1;
            this.musicButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.musicButtons.Size = new System.Drawing.Size(584, 44);
            this.musicButtons.TabIndex = 0;
            // 
            // play
            // 
            this.play.ButtonImage = ((System.Drawing.Image)(resources.GetObject("play.ButtonImage")));
            this.play.Dock = System.Windows.Forms.DockStyle.Fill;
            this.play.Location = new System.Drawing.Point(270, 3);
            this.play.Name = "play";
            this.play.Size = new System.Drawing.Size(44, 38);
            this.play.TabIndex = 0;
            // 
            // previousTrack
            // 
            this.previousTrack.ButtonImage = global::Player.Properties.Resources.previous;
            this.previousTrack.Location = new System.Drawing.Point(220, 3);
            this.previousTrack.Name = "previousTrack";
            this.previousTrack.Size = new System.Drawing.Size(44, 38);
            this.previousTrack.TabIndex = 1;
            // 
            // nextTrack
            // 
            this.nextTrack.ButtonImage = global::Player.Properties.Resources.next;
            this.nextTrack.Location = new System.Drawing.Point(320, 3);
            this.nextTrack.Name = "nextTrack";
            this.nextTrack.Size = new System.Drawing.Size(44, 38);
            this.nextTrack.TabIndex = 2;
            // 
            // trackBarArea
            // 
            this.trackBarArea.BackColor = System.Drawing.Color.Transparent;
            this.trackBarArea.ColumnCount = 3;
            this.trackBarArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.trackBarArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 500F));
            this.trackBarArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.trackBarArea.Controls.Add(this.endTime, 2, 0);
            this.trackBarArea.Controls.Add(this.currentTime, 0, 0);
            this.trackBarArea.Controls.Add(this.trackBarItem, 1, 0);
            this.trackBarArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarArea.Location = new System.Drawing.Point(3, 53);
            this.trackBarArea.Name = "trackBarArea";
            this.trackBarArea.RowCount = 1;
            this.trackBarArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.trackBarArea.Size = new System.Drawing.Size(584, 24);
            this.trackBarArea.TabIndex = 1;
            // 
            // endTime
            // 
            this.endTime.AutoSize = true;
            this.endTime.BackColor = System.Drawing.Color.Transparent;
            this.endTime.Dock = System.Windows.Forms.DockStyle.Left;
            this.endTime.ForeColor = System.Drawing.Color.DarkGray;
            this.endTime.Location = new System.Drawing.Point(545, 0);
            this.endTime.Name = "endTime";
            this.endTime.Size = new System.Drawing.Size(34, 24);
            this.endTime.TabIndex = 2;
            this.endTime.Text = "00:00";
            this.endTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // currentTime
            // 
            this.currentTime.AutoSize = true;
            this.currentTime.BackColor = System.Drawing.Color.Transparent;
            this.currentTime.Dock = System.Windows.Forms.DockStyle.Right;
            this.currentTime.ForeColor = System.Drawing.Color.DarkGray;
            this.currentTime.Location = new System.Drawing.Point(5, 0);
            this.currentTime.Name = "currentTime";
            this.currentTime.Size = new System.Drawing.Size(34, 24);
            this.currentTime.TabIndex = 1;
            this.currentTime.Text = "00:00";
            this.currentTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // trackBarItem
            // 
            this.trackBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.trackBarItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarItem.Location = new System.Drawing.Point(45, 3);
            this.trackBarItem.Name = "trackBarItem";
            this.trackBarItem.ProgressLineColor = System.Drawing.Color.White;
            this.trackBarItem.Size = new System.Drawing.Size(494, 18);
            this.trackBarItem.TabIndex = 3;
            // 
            // volumeArea
            // 
            this.volumeArea.ColumnCount = 1;
            this.volumeArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.volumeArea.Controls.Add(this.musicVolume, 0, 0);
            this.volumeArea.Dock = System.Windows.Forms.DockStyle.Right;
            this.volumeArea.Location = new System.Drawing.Point(810, 3);
            this.volumeArea.Name = "volumeArea";
            this.volumeArea.RowCount = 1;
            this.volumeArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.volumeArea.Size = new System.Drawing.Size(183, 79);
            this.volumeArea.TabIndex = 1;
            // 
            // musicVolume
            // 
            this.musicVolume.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicVolume.Location = new System.Drawing.Point(3, 3);
            this.musicVolume.Name = "musicVolume";
            this.musicVolume.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            this.musicVolume.Size = new System.Drawing.Size(177, 73);
            this.musicVolume.TabIndex = 0;
            // 
            // musicPlayTimer
            // 
            this.musicPlayTimer.Tick += new System.EventHandler(this.musicPlayTimer_Tick);
            // 
            // MusicTrackBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.Controls.Add(this.musicTrackBarArea);
            this.Name = "MusicTrackBar";
            this.Size = new System.Drawing.Size(996, 85);
            this.musicTrackBarArea.ResumeLayout(false);
            this.playlistArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.currentTrackImage)).EndInit();
            this.textArea.ResumeLayout(false);
            this.textArea.PerformLayout();
            this.musicControlsArea.ResumeLayout(false);
            this.musicButtons.ResumeLayout(false);
            this.trackBarArea.ResumeLayout(false);
            this.trackBarArea.PerformLayout();
            this.volumeArea.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel musicTrackBarArea;
        private TableLayoutPanel musicControlsArea;
        private TableLayoutPanel musicButtons;
        private TableLayoutPanel trackBarArea;
        private Label endTime;
        private Label currentTime;
        private System.Windows.Forms.Timer musicPlayTimer;
        private TrackBarItem trackBarItem;
        private TableLayoutPanel volumeArea;
        private MusicVolume musicVolume;
        private CustomButton play;
        private CustomButton previousTrack;
        private CustomButton nextTrack;
        private TableLayoutPanel playlistArea;
        private PictureBox currentTrackImage;
        private TableLayoutPanel textArea;
        private Label title;
    }
}
