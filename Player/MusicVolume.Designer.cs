﻿namespace Player
{
    partial class MusicVolume
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.volumeArea = new System.Windows.Forms.TableLayoutPanel();
            this.volumePicture = new System.Windows.Forms.PictureBox();
            this.trackBarItem = new Player.TrackBarItem();
            this.volumeArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.volumePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // volumeArea
            // 
            this.volumeArea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.volumeArea.ColumnCount = 2;
            this.volumeArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.volumeArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.volumeArea.Controls.Add(this.volumePicture, 0, 0);
            this.volumeArea.Controls.Add(this.trackBarItem, 1, 0);
            this.volumeArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.volumeArea.Location = new System.Drawing.Point(0, 0);
            this.volumeArea.Name = "volumeArea";
            this.volumeArea.Padding = new System.Windows.Forms.Padding(12);
            this.volumeArea.RowCount = 1;
            this.volumeArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.volumeArea.Size = new System.Drawing.Size(200, 60);
            this.volumeArea.TabIndex = 0;
            // 
            // volumePicture
            // 
            this.volumePicture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.volumePicture.Image = global::Player.Properties.Resources.unmute;
            this.volumePicture.Location = new System.Drawing.Point(15, 15);
            this.volumePicture.Name = "volumePicture";
            this.volumePicture.Size = new System.Drawing.Size(44, 30);
            this.volumePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.volumePicture.TabIndex = 0;
            this.volumePicture.TabStop = false;
            // 
            // trackBarItem
            // 
            this.trackBarItem.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.trackBarItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBarItem.Location = new System.Drawing.Point(65, 15);
            this.trackBarItem.Name = "trackBarItem";
            this.trackBarItem.Padding = new System.Windows.Forms.Padding(0, 6, 0, 6);
            this.trackBarItem.ProgressLineColor = System.Drawing.Color.White;
            this.trackBarItem.Size = new System.Drawing.Size(120, 30);
            this.trackBarItem.TabIndex = 1;
            // 
            // MusicVolume
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.volumeArea);
            this.Name = "MusicVolume";
            this.Size = new System.Drawing.Size(200, 60);
            this.volumeArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.volumePicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel volumeArea;
        private PictureBox volumePicture;
        private TrackBarItem trackBarItem;
    }
}
