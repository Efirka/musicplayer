﻿namespace Player
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.musicItemArea = new System.Windows.Forms.FlowLayoutPanel();
            this.trackBar = new Player.MusicTrackBar();
            this.playlistArea = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // musicItemArea
            // 
            this.musicItemArea.AutoScroll = true;
            this.musicItemArea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.musicItemArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.musicItemArea.Location = new System.Drawing.Point(225, 0);
            this.musicItemArea.Name = "musicItemArea";
            this.musicItemArea.Size = new System.Drawing.Size(767, 365);
            this.musicItemArea.TabIndex = 0;
            // 
            // trackBar
            // 
            this.trackBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(25)))), ((int)(((byte)(25)))));
            this.trackBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.trackBar.Location = new System.Drawing.Point(0, 365);
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(992, 85);
            this.trackBar.TabIndex = 1;
            // 
            // playlistArea
            // 
            this.playlistArea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.playlistArea.Dock = System.Windows.Forms.DockStyle.Left;
            this.playlistArea.Location = new System.Drawing.Point(0, 0);
            this.playlistArea.Name = "playlistArea";
            this.playlistArea.Size = new System.Drawing.Size(225, 365);
            this.playlistArea.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(992, 450);
            this.Controls.Add(this.musicItemArea);
            this.Controls.Add(this.playlistArea);
            this.Controls.Add(this.trackBar);
            this.Name = "Form1";
            this.Text = "CatsPlayer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private FlowLayoutPanel musicItemArea;
        private MusicTrackBar trackBar;
        private FlowLayoutPanel playlistArea;
    }
}