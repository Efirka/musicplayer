﻿namespace Player
{
    partial class MusicPlaylist
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.playlistArea = new System.Windows.Forms.TableLayoutPanel();
            this.playlistImage = new System.Windows.Forms.PictureBox();
            this.textArea = new System.Windows.Forms.TableLayoutPanel();
            this.date = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.Label();
            this.playlistArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.playlistImage)).BeginInit();
            this.textArea.SuspendLayout();
            this.SuspendLayout();
            // 
            // playlistArea
            // 
            this.playlistArea.ColumnCount = 2;
            this.playlistArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.playlistArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playlistArea.Controls.Add(this.playlistImage, 0, 0);
            this.playlistArea.Controls.Add(this.textArea, 1, 0);
            this.playlistArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playlistArea.Location = new System.Drawing.Point(0, 0);
            this.playlistArea.Name = "playlistArea";
            this.playlistArea.Padding = new System.Windows.Forms.Padding(10);
            this.playlistArea.RowCount = 1;
            this.playlistArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playlistArea.Size = new System.Drawing.Size(230, 80);
            this.playlistArea.TabIndex = 0;
            // 
            // playlistImage
            // 
            this.playlistImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playlistImage.Image = global::Player.Properties.Resources.images;
            this.playlistImage.Location = new System.Drawing.Point(10, 10);
            this.playlistImage.Margin = new System.Windows.Forms.Padding(0);
            this.playlistImage.Name = "playlistImage";
            this.playlistImage.Size = new System.Drawing.Size(60, 60);
            this.playlistImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.playlistImage.TabIndex = 0;
            this.playlistImage.TabStop = false;
            // 
            // textArea
            // 
            this.textArea.ColumnCount = 1;
            this.textArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.textArea.Controls.Add(this.date, 0, 1);
            this.textArea.Controls.Add(this.title, 0, 0);
            this.textArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textArea.Location = new System.Drawing.Point(70, 10);
            this.textArea.Margin = new System.Windows.Forms.Padding(0);
            this.textArea.Name = "textArea";
            this.textArea.RowCount = 2;
            this.textArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.textArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.textArea.Size = new System.Drawing.Size(150, 60);
            this.textArea.TabIndex = 1;
            // 
            // date
            // 
            this.date.AutoSize = true;
            this.date.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.date.Font = new System.Drawing.Font("Candara", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.date.ForeColor = System.Drawing.SystemColors.Control;
            this.date.Location = new System.Drawing.Point(3, 30);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(144, 30);
            this.date.TabIndex = 1;
            this.date.Text = "Date";
            this.date.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // title
            // 
            this.title.AutoSize = true;
            this.title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.title.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.title.ForeColor = System.Drawing.SystemColors.Control;
            this.title.Location = new System.Drawing.Point(3, 0);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(144, 30);
            this.title.TabIndex = 0;
            this.title.Text = "Title";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MusicPlaylist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.Controls.Add(this.playlistArea);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MusicPlaylist";
            this.Size = new System.Drawing.Size(230, 80);
            this.playlistArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.playlistImage)).EndInit();
            this.textArea.ResumeLayout(false);
            this.textArea.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel playlistArea;
        private PictureBox playlistImage;
        private TableLayoutPanel textArea;
        private Label title;
        private Label date;
    }
}
