﻿using NAudio.Gui;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;

namespace Player
{
    public partial class MusicItem : UserControl
    {
        EventHandler? playMusic;

        public event EventHandler? PlayMusic
        {
            add
            {
                playMusic += value;
            }
            remove
            {
                playMusic -= value;
            }
        }

        string file;
       

        public MusicItem()
        {
            
            InitializeComponent();

            HoverRecursive(musicArea);
            ClickRecursive(musicArea);
            DoubleClickRecursive(musicArea);
        }

        public MusicItem(string filePath) : this()
        {
            FileInfo file = new FileInfo(filePath);
            TrackPath = file.FullName;            
            Title = file.Name.Replace(".mp3", "");
            Description = file.CreationTime.ToString("♥ yyyy MMMM");

            string pathImage = Path.Combine(file.Directory!.FullName, "Images", Title + ".jpg");
            if (File.Exists(pathImage))
            {
                TrackImage = Image.FromFile(pathImage);
            }
        }


        #region Hover
        void HoverRecursive(Control element)
        {
            element.MouseEnter += Element_MouseEnter;
            element.MouseLeave += Element_MouseLeave;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                HoverRecursive(element.Controls[i]);
            }
        }

        private void Element_MouseLeave(object? sender, EventArgs e)
        {
            BackColor = Color.FromArgb(50, 50, 50);
        }

        private void Element_MouseEnter(object? sender, EventArgs e)
        {
            BackColor = Color.FromArgb(60, 60, 60);
        }
        #endregion
        #region Click
        void ClickRecursive(Control element)
        {
            element.MouseDown += Element_MouseDown;
            element.MouseUp += Element_MouseUp;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                ClickRecursive(element.Controls[i]);
            }
        }

        private void Element_MouseUp(object? sender, MouseEventArgs e)
        {
            BackColor = Color.FromArgb(60, 60, 60);
        }

        private void Element_MouseDown(object? sender, MouseEventArgs e)
        {
            BackColor = Color.FromArgb(70, 70, 70);
        }

        #endregion
        #region DoubleClick
        void DoubleClickRecursive(Control element)
        {
            element.MouseDoubleClick += Element_MouseDoubleClick;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                DoubleClickRecursive(element.Controls[i]);
            }
        }

        private void Element_MouseDoubleClick(object? sender, MouseEventArgs e)
        {
            if (playMusic != null)
            {
                playMusic.Invoke(this, EventArgs.Empty);
            }
                
        }


        #endregion
        public string TrackPath
        {
            get
            {
                if (file == null)
                {
                    return "null";
                }
                return file;
            }
            set
            {
                file = value;
            }
        }

      

        [Category("Властивості треку")]
        public string Title
        {
            get
            {
                return title.Text;
            }
            set
            {
                title.Text = value;
            }
        }

        [Category("Властивості треку")]
        public string Description
        {
            get
            {
                return description.Text;
            }
            set
            {
                description.Text = value;
            }
        }

        [Category("Властивості треку")]
        public Image TrackImage
        {
            get
            {
                return musicItemImage.Image;
            }
            set
            {
                musicItemImage.Image = value;
            }
        }
    }
}
