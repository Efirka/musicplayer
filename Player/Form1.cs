﻿using NAudio.Wave;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;

namespace Player
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            trackBar.Next += TrackBar_Next;
            trackBar.Prev += TrackBar_Prev;
        }

        private void TrackBar_Prev(object? sender, EventArgs e)
        {
            MusicItem playlist = (MusicItem)sender!;
            int index = musicItemArea.Controls.IndexOf(playlist);
            if (index > 0)
            {
                index--;
            }
            else
            {
                index = musicItemArea.Controls.Count - 1;
            }
            var prev = (MusicItem)musicItemArea.Controls[index];
            trackBar.PlayPause(prev);
        }

        private void TrackBar_Next(object? sender, EventArgs e)
        {
            MusicItem playlist = (MusicItem)sender!;
            int index = musicItemArea.Controls.IndexOf(playlist);
            if (index < musicItemArea.Controls.Count - 1)
            {
                index++;
            }
            else
            {
                index = 0;
            }
            var next = (MusicItem)musicItemArea.Controls[index];
            trackBar.PlayPause(next);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string [] folders = Directory.GetDirectories("Music");
            foreach (string folder in folders) 
            {
                var playlist = new MusicPlaylist(folder);
                playlist.ShowTracks += Playlist_ShowTracks;
                playlistArea.Controls.Add(playlist);
            }
            
            if (folders.Length > 0) 
            {
                ShowTracks(folders[0]);
            }
        }

        void ShowTracks(string folder)
        {
            musicItemArea.Controls.Clear();
            string[] tracks = Directory.GetFiles(folder, "*.mp3");

            foreach (var track in tracks)
            {
               
                    var musicTrack = new MusicItem(track);
                musicTrack.PlayMusic += Track_PlayMusic;
                    musicItemArea.Controls.Add(musicTrack);
                
            }
           
        }

        private void Playlist_ShowTracks(object? sender, EventArgs e)
        {
            MusicPlaylist playlist = (MusicPlaylist)sender!;
            ShowTracks(playlist.Folder);
            
        }

        private void Track_PlayMusic(object? sender, EventArgs e)
        {
            MusicItem music = (MusicItem)sender!;
            trackBar.PlayPause(music);
        }
    }
}