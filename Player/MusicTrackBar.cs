﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Player
{
    public partial class MusicTrackBar : UserControl
    {
        WaveOut wave;
        AudioFileReader audio;
       
        bool isPlay = false;

        EventHandler? next;

        public event EventHandler? Next
        {
            add
            {
                next += value;
            }
            remove
            {
                next -= value;
            }
        }

        EventHandler? prev;

        public event EventHandler? Prev
        {
            add
            {
                prev += value;
            }
            remove
            {
                prev -= value;
            }
        }
        public MusicItem LastTrack { get; private set; }
        public MusicTrackBar()
        {
            InitializeComponent();
            HoverRecursive(trackBarArea);
            trackBarItem.ValueChanged += TrackBarItem_ValueChanged;
            musicVolume.ValueChanged += MusicVolume_ValueChanged;
            play.CustomClick += Play_Clicked;
            nextTrack.CustomClick += NextTrack_CustomClick;
            previousTrack.CustomClick += PreviousTrack_CustomClick;
        }

        private void PreviousTrack_CustomClick(object? sender, EventArgs e)
        {
            if (prev != null)
            {
                prev.Invoke(LastTrack, e);
            }
        }

        private void NextTrack_CustomClick(object? sender, EventArgs e)
        {
            if (next != null)
            {
                next.Invoke(LastTrack, e); 
            }
        }

        private void MusicVolume_ValueChanged(object? sender, EventArgs e)
        {
            if (audio!= null )
            {
                audio.Volume = (float)sender!;
            }
            
        }

        private void Play_Clicked(object? sender, EventArgs e)
        {
            try
            {
                if (isPlay)
                {
                    wave.Pause();
                    musicPlayTimer.Stop();
                    play.ButtonImage = Properties.Resources.play;
                }
                else
                {
                    wave.Play();
                    musicPlayTimer.Start();
                    play.ButtonImage = Properties.Resources.pause;
                }
                isPlay = !isPlay;
            }
            catch { }
        }

        private void TrackBarItem_ValueChanged(object? sender, EventArgs e)
        {
            double seconds = (double)sender!;
            try
            { 
                audio.CurrentTime = TimeSpan.FromSeconds(seconds); 
            }
            catch (Exception) { }
        }


        public void PlayPause(MusicItem track)
        {
            try
            {
                LastTrack = track;
                if (wave != null)
                {
                    wave.Dispose();
                    audio.Dispose();
                }
                wave = new WaveOut();
                audio = new AudioFileReader(LastTrack.TrackPath);
                Play();
            }
            catch { }
           
        }

        void Play()
        {
            wave.Init(audio);
            wave.Play();
            isPlay = true;
            musicPlayTimer.Start();
            
            TimeSpan end = audio.TotalTime;
            endTime.Text = $"{end:mm}:{end:ss}";
            trackBarItem.SetDuration(end);

            audio.Volume = musicVolume.Value;
            play.ButtonImage = Properties.Resources.pause;

            title.Text = LastTrack.Title;
            currentTrackImage.Image = LastTrack.TrackImage;
            wave.PlaybackStopped += Wave_PlaybackStopped;
            
        }

        private void Wave_PlaybackStopped(object? sender, StoppedEventArgs e)
        {
           if (next != null)
            {
                next.Invoke(LastTrack,e);
            }
        }

        private void musicPlayTimer_Tick(object sender, EventArgs e)
        {
            TimeSpan current = audio.CurrentTime;
            currentTime.Text = $"{current:mm}:{current:ss}";

            float timePercent = (float)(audio.CurrentTime / audio.TotalTime * 100);
           trackBarItem.Value = timePercent;

        }

        #region Hover
        void HoverRecursive(Control element)
        {
            element.MouseEnter += Element_MouseEnter;
            element.MouseLeave += Element_MouseLeave;

            foreach (Control child in element.Controls)
            {
                HoverRecursive(child);
            }
        }

        private void Element_MouseLeave(object? sender, EventArgs e)
        {
            trackBarItem.ProgressLineColor = Color.Silver;
        }

        private void Element_MouseEnter(object? sender, EventArgs e)
        {
            trackBarItem.ProgressLineColor = Color.White;
        }
        #endregion


    }
}
