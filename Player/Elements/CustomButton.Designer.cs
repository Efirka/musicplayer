﻿namespace Player
{
    partial class CustomButton
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.imageArea = new System.Windows.Forms.TableLayoutPanel();
            this.buttonImage = new System.Windows.Forms.PictureBox();
            this.imageArea.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonImage)).BeginInit();
            this.SuspendLayout();
            // 
            // imageArea
            // 
            this.imageArea.ColumnCount = 1;
            this.imageArea.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.imageArea.Controls.Add(this.buttonImage, 0, 0);
            this.imageArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageArea.Location = new System.Drawing.Point(0, 0);
            this.imageArea.Margin = new System.Windows.Forms.Padding(0);
            this.imageArea.Name = "imageArea";
            this.imageArea.Padding = new System.Windows.Forms.Padding(3);
            this.imageArea.RowCount = 1;
            this.imageArea.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.imageArea.Size = new System.Drawing.Size(50, 50);
            this.imageArea.TabIndex = 0;
            // 
            // buttonImage
            // 
            this.buttonImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonImage.Image = global::Player.Properties.Resources.play;
            this.buttonImage.Location = new System.Drawing.Point(3, 3);
            this.buttonImage.Margin = new System.Windows.Forms.Padding(0);
            this.buttonImage.Name = "buttonImage";
            this.buttonImage.Size = new System.Drawing.Size(44, 44);
            this.buttonImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.buttonImage.TabIndex = 0;
            this.buttonImage.TabStop = false;
            // 
            // CustomButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.imageArea);
            this.Name = "CustomButton";
            this.Size = new System.Drawing.Size(50, 50);
            this.imageArea.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel imageArea;
        private PictureBox buttonImage;
    }
}
