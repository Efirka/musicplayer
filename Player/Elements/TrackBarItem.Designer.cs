﻿namespace Player
{
    partial class TrackBarItem
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackBar = new System.Windows.Forms.TableLayoutPanel();
            this.whiteLine = new System.Windows.Forms.Label();
            this.grayLine = new System.Windows.Forms.Label();
            this.trackBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // trackBar
            // 
            this.trackBar.ColumnCount = 2;
            this.trackBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.trackBar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.trackBar.Controls.Add(this.whiteLine, 0, 0);
            this.trackBar.Controls.Add(this.grayLine, 1, 0);
            this.trackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.trackBar.Location = new System.Drawing.Point(0, 0);
            this.trackBar.Name = "trackBar";
            this.trackBar.Padding = new System.Windows.Forms.Padding(0, 7, 0, 7);
            this.trackBar.RowCount = 1;
            this.trackBar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.trackBar.Size = new System.Drawing.Size(721, 20);
            this.trackBar.TabIndex = 0;
            this.trackBar.MouseClick += new System.Windows.Forms.MouseEventHandler(this.trackBar_MouseClick);
            // 
            // whiteLine
            // 
            this.whiteLine.AutoSize = true;
            this.whiteLine.BackColor = System.Drawing.Color.Silver;
            this.whiteLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.whiteLine.Location = new System.Drawing.Point(0, 7);
            this.whiteLine.Margin = new System.Windows.Forms.Padding(0);
            this.whiteLine.Name = "whiteLine";
            this.whiteLine.Size = new System.Drawing.Size(216, 6);
            this.whiteLine.TabIndex = 0;
            this.whiteLine.MouseClick += new System.Windows.Forms.MouseEventHandler(this.trackBar_MouseClick);
            // 
            // grayLine
            // 
            this.grayLine.AutoSize = true;
            this.grayLine.BackColor = System.Drawing.Color.DimGray;
            this.grayLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grayLine.Location = new System.Drawing.Point(216, 7);
            this.grayLine.Margin = new System.Windows.Forms.Padding(0);
            this.grayLine.Name = "grayLine";
            this.grayLine.Size = new System.Drawing.Size(505, 6);
            this.grayLine.TabIndex = 1;
            this.grayLine.MouseClick += new System.Windows.Forms.MouseEventHandler(this.grayLine_MouseClick);
            // 
            // TrackBarItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.Controls.Add(this.trackBar);
            this.Name = "TrackBarItem";
            this.Size = new System.Drawing.Size(721, 20);
            this.trackBar.ResumeLayout(false);
            this.trackBar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel trackBar;
        private Label whiteLine;
        private Label grayLine;
    }
}
