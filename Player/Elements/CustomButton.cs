﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Player
{
    public partial class CustomButton : UserControl
    {
        EventHandler? customClick;

        public event EventHandler CustomClick
        {
            add { customClick += value; }
            remove { customClick -= value; }
        }

        public CustomButton()
        {
            InitializeComponent();
            ClickRecursive(this);
           
            buttonImage.MouseEnter += Element_MouseEnter;
            buttonImage.MouseLeave += Element_MouseLeave;
        }

        [Category("Властивості кнопки")]
        public Image ButtonImage
        {
            get
            {
                return buttonImage.Image;
            }
            set
            {
                buttonImage.Image = value;
            }
        }

        #region Click
        void ClickRecursive(Control element)
        {
            element.Click += Element_Click;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                ClickRecursive(element.Controls[i]);
            }
        }

        private void Element_Click(object? sender, EventArgs e)
        {
            try
            {
                if (customClick != null)
                {
                    customClick.Invoke(this, e);
                }
            }
            catch { }
        }


        #endregion

        #region Hover
        
        private void Element_MouseLeave(object? sender, EventArgs e)
        {
            imageArea.Padding = new Padding(3);
        }

        private void Element_MouseEnter(object? sender, EventArgs e)
        {
            imageArea.Padding = new Padding(0);
        }
        #endregion
    }
}
