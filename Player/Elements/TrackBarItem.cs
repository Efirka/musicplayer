﻿using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Player
{
    public partial class TrackBarItem : UserControl
    {
        EventHandler? valueChange;
        public double max;
        
        
        public event EventHandler ValueChanged
        {
            add { valueChange += value; }
            remove { valueChange -= value; }
        }

        public float Value 
        {
            
            set
            {
                trackBar.ColumnStyles[0].Width = value;
                trackBar.ColumnStyles[1].Width = 100 - value;
            }
        
        }
        public TrackBarItem()
        {
            InitializeComponent();
        }

        public void SetDuration(double max)
        {
            this.max = max;
        }
        public void SetDuration(TimeSpan max)
        {
            this.max = max.TotalSeconds;
        }
        
        private void trackBar_MouseClick(object sender, MouseEventArgs e)
        {
            double percent = (double)e.X / trackBar.Width;
            double resultTime = max * percent;
            if (valueChange != null) 
            { 
                valueChange.Invoke(resultTime, EventArgs.Empty); 
            }
            
        }

        private void grayLine_MouseClick(object sender, MouseEventArgs e)
        {
            double percent = (double)(e.X + whiteLine.Width) / trackBar.Width;
            double resultTime = max * percent;
            if (valueChange != null)
            {
                valueChange.Invoke(resultTime, EventArgs.Empty);
            }
        }

        public Color ProgressLineColor
        {
            get
            {
                return whiteLine.BackColor;
            }
            set
            {
                whiteLine.BackColor = value;
            }
        }
    }
}
