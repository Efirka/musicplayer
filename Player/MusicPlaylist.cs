﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Player
{
    public partial class MusicPlaylist : UserControl
    {
        EventHandler? showTracks;

        public event EventHandler? ShowTracks
        {
            add
            {
                showTracks += value;
            }
            remove
            {
                showTracks -= value;
            }
        }

        public MusicPlaylist(string folder)
        {
            InitializeComponent();
            Folder = folder;
            DirectoryInfo directory = new DirectoryInfo(folder);
            Title = directory.Name;
            Date = directory.CreationTime.ToString("♥ yyyy MMMM");
            string pathImage = Path.Combine(folder, Title + ".jpg");
            if (File.Exists(pathImage))
            {
                PlaylistImage = Image.FromFile(pathImage);
            }
            

            HoverRecursive(this);
            ClickRecursive(this); 
            DoubleClickRecursive(this);

        }

        [Category("Властивості плейліста")]
        public string Title
        {
            get
            {
                return title.Text;
            }
            set
            {
                title.Text = value;
            }
        }

        [Category("Властивості плейліста")]
        public string Date
        {
            get
            {
                return date.Text;
            }
            set
            {
                date.Text = value;
            }
        }

        [Category("Властивості плейліста")]
        public Image PlaylistImage
        {
            get
            {
                return playlistImage.Image;
            }
            set
            {
                playlistImage.Image = value;
            }
        }


        public string Folder { get; set; }

        #region Hover
        void HoverRecursive(Control element)
        {
            element.MouseEnter += Element_MouseEnter;
            element.MouseLeave += Element_MouseLeave;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                HoverRecursive(element.Controls[i]);
            }
        }

        private void Element_MouseLeave(object? sender, EventArgs e)
        {
            BackColor = Color.FromArgb(20, 20, 20);
        }

        private void Element_MouseEnter(object? sender, EventArgs e)
        {
            BackColor = Color.FromArgb(30, 30, 30);
        }
        #endregion
        #region Click
        void ClickRecursive(Control element)
        {
            element.MouseDown += Element_MouseDown;
            element.MouseUp += Element_MouseUp;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                ClickRecursive(element.Controls[i]);
            }
        }

        private void Element_MouseUp(object? sender, MouseEventArgs e)
        {
            BackColor = Color.FromArgb(30, 30, 30);
        }

        private void Element_MouseDown(object? sender, MouseEventArgs e)
        {
            BackColor = Color.FromArgb(40, 40, 40);
        }

        #endregion
        #region DoubleClick
        void DoubleClickRecursive(Control element)
        {
            element.MouseDoubleClick += Element_MouseDoubleClick;

            for (int i = 0; i < element.Controls.Count; i++)
            {
                DoubleClickRecursive(element.Controls[i]);
            }
        }

        private void Element_MouseDoubleClick(object? sender, MouseEventArgs e)
        {
            if (showTracks != null)
            {
                showTracks.Invoke(this, EventArgs.Empty);
            }

        }


        #endregion
    }
}
